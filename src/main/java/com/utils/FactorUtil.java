package com.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FactorUtil {
    static SessionFactory factory=null;
    static {
        //加载配置文件
        Configuration cfg=new Configuration();
        cfg.configure();
        //创建session工厂
        factory= cfg.buildSessionFactory();

    }
    public static SessionFactory getFactory(){
        return factory;
    }
    //获得当前session
    public static Session getSession(){
        return factory.getCurrentSession();
    }
}
