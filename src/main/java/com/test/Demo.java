package com.test;

import com.entity.Ads;
import com.utils.FactorUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;

public class Demo {
    @Test
    public void test(){
        SessionFactory factory = FactorUtil.getFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        Ads address = session.get(Ads.class, 1);
        System.out.println(address);
        transaction.commit();
        session.close();
        factory.close();
    }
}
