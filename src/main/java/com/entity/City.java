package com.entity;

public class City {
    private  Integer id;
    private  String city;
    private  Integer pid;

    public City() {
    }

    public City(String city, Integer pid) {
        this.city = city;
        this.pid = pid;
    }

    public City(Integer id, String city, Integer pid) {
        this.id = id;
        this.city = city;
        this.pid = pid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
}
