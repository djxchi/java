package com.entity;

public class Address {
    private  Integer id;
    private  String name;
    private  String address;
    private  String tel;
    private  int user_id;

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", tel='" + tel + '\'' +
                ", user_id=" + user_id +
                '}';
    }

    public Address() {
    }

    public Address(String name, String address, String tel, int user_id) {
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.user_id = user_id;
    }

    public Address(Integer id, String name, String address, String tel, int user_id) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.tel = tel;
        this.user_id = user_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
