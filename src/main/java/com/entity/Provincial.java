package com.entity;

public class Provincial {
    private  Integer id;
    private  String provincial;

    public Provincial() {
    }

    public Provincial(Integer id, String provincial) {
        this.id = id;
        this.provincial = provincial;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProvincial() {
        return provincial;
    }

    public void setProvincial(String provincial) {
        this.provincial = provincial;
    }
}
