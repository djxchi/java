package com.entity;

public class Product {
    private  Integer id;
    private  String name;
    private  String url;
    private  double price;
    private  int stock;
    private  String detail;
    private  int category_id;

    public Product() {
    }

    public Product(String name, String url, double price, int stock, String detail, int category_id) {
        this.name = name;
        this.url = url;
        this.price = price;
        this.stock = stock;
        this.detail = detail;
        this.category_id = category_id;
    }

    public Product(Integer id, String name, String url, double price, int stock, String detail, int category_id) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.price = price;
        this.stock = stock;
        this.detail = detail;
        this.category_id = category_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}
