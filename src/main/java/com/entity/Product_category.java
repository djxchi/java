package com.entity;

public class Product_category {
    private  Integer id;
    private  String name;
    private  int parent_id;

    public Product_category() {
    }

    public Product_category(String name, int parent_id) {
        this.name = name;
        this.parent_id = parent_id;
    }

    public Product_category(Integer id, String name, int parent_id) {
        this.id = id;
        this.name = name;
        this.parent_id = parent_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }
}
