package com.entity;

import java.sql.Date;

public class User {
    private  Integer id;
    private  String login;
    private String name;
    private  String pwd;
    private  String sex;
    private  String phone;
    private  String photo;
    private Date birthday;
    private  Integer isMaster;
    private  int age;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                ", sex='" + sex + '\'' +
                ", phone='" + phone + '\'' +
                ", photo='" + photo + '\'' +
                ", birthday=" + birthday +
                ", isMaster=" + isMaster +
                ", age=" + age +
                '}';
    }

    public User() {
    }

    public User(Integer id, String login, String name, String pwd, String sex, String phone, String photo, Date birthday, Integer isMaster, int age) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.pwd = pwd;
        this.sex = sex;
        this.phone = phone;
        this.photo = photo;
        this.birthday = birthday;
        this.isMaster = isMaster;
        this.age = age;
    }

    public User(String login, String name, String pwd, String sex, String phone, String photo, Date birthday, Integer isMaster, int age) {
        this.login = login;
        this.name = name;
        this.pwd = pwd;
        this.sex = sex;
        this.phone = phone;
        this.photo = photo;
        this.birthday = birthday;
        this.isMaster = isMaster;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Integer isMaster) {
        this.isMaster = isMaster;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
