package com.entity;

import com.sun.xml.bind.v2.model.core.ID;

import java.sql.Date;

public class News {
    private Integer id;
    private  String title;
    private  String content;
    private Date creation_time;

    public News() {
    }

    public News(String title, String content, Date creation_time) {
        this.title = title;
        this.content = content;
        this.creation_time = creation_time;
    }

    public News(Integer id, String title, String content, Date creation_time) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.creation_time = creation_time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Date creation_time) {
        this.creation_time = creation_time;
    }
}
