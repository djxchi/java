package com.entity;

public class Ads {
    private  Integer id;
    private  String title;
    private  String remark;
    private  String img;
    private  String url;
    private  String target;

    public Ads() {
    }

    public Ads(String title, String remark, String img, String url, String target) {
        this.title = title;
        this.remark = remark;
        this.img = img;
        this.url = url;
        this.target = target;
    }

    public Ads(Integer id, String title, String remark, String img, String url, String target) {
        this.id = id;
        this.title = title;
        this.remark = remark;
        this.img = img;
        this.url = url;
        this.target = target;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
}
