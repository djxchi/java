package com.entity;

import java.io.PipedReader;
import java.sql.Date;

public class Order {
    private  Integer id;
    private  String user_name;
    private  String address;
    private Date creation_time;
    private  String tel;
    private  int total_amount;
    private int status;
    private  int user_id;

    public Order() {
    }

    public Order(String user_name, String address, Date creation_time, String tel, int total_amount, int status, int user_id) {
        this.user_name = user_name;
        this.address = address;
        this.creation_time = creation_time;
        this.tel = tel;
        this.total_amount = total_amount;
        this.status = status;
        this.user_id = user_id;
    }

    public Order(Integer id, String user_name, String address, Date creation_time, String tel, int total_amount, int status, int user_id) {
        this.id = id;
        this.user_name = user_name;
        this.address = address;
        this.creation_time = creation_time;
        this.tel = tel;
        this.total_amount = total_amount;
        this.status = status;
        this.user_id = user_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(Date creation_time) {
        this.creation_time = creation_time;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
}
