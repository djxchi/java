<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>快乐购 - 用户管理</title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="../css/normalize.min.css" />
		<link rel="stylesheet" type="text/css" href="../css/default.css" />

		<!-- JS -->
		<script src="../js/jquery-3.2.1.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="../js/default.js" type="text/javascript" charset="utf-8"></script>

		<!--[if lt IE 9]>
			<script src="../js/html5shiv.min.js" type="text/javascript" charset="utf-8"></script>
			<script src="../js/respond.min.js" type="text/javascript" charset="utf-8"></script>
		<![endif]-->
	</head>

	<body>
		<!-- 导航条开始 -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<a href="#navbar-menu1" data-toggle="collapse" class="navbar-toggle collapsed">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a href="../index.jsp" class="navbar-brand">
						<img src="../img/logo.png" alt="Happy Go" />
					</a>
				</div>
				<div id="navbar-menu1" class="navbar-collapse collapse">
					<div class="navbar-text">
						<span>Admin，</span>
						<span>欢迎来到快乐购管理页面！&nbsp;</span>
						<div class="visible-xs"></div>
						<span>[<a href="login.jsp">注销</a>]&nbsp;</span>
					</div>

					<div class="navbar-right">
						<ul class="navbar-nav nav">
							<li>
								<a href="index.jsp">管理首页</a>
							</li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown">
									<span>商品</span>
									<span class="caret"></span>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="product-category.jsp">类别管理</a>
									</li>
									<li>
										<a href="product.jsp">商品管理</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="order.jsp">订单</a>
							</li>
							<li class="active">
								<a href="user.jsp">用户</a>
							</li>
							<li>
								<a href="news.jsp">新闻</a>
							</li>
							<li>
								<a href="ads.jsp">广告</a>
							</li>
							<li>
								<a href="../index.jsp">购物首页</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<!-- //导航条结束 -->

		<!-- 页面主体部分 -->
		<div class="container">
			<div class="h3 page-header">
				<span>用户管理</span>
				<small class="pull-right">
					<a href="#modal-add-user" class="btn btn-xs btn-primary" data-toggle="modal">新增用户</a>
				</small>
			</div>

			<table class="table table-hover table-striped">
				<thead class="bg-primary">
					<tr>
						<th class="hidden-xs">#</th>
						<th>用户名</th>
						<th>姓名</th>
						<th class="hidden-xs">性别</th>
						<th class="hidden-xs hidden-sm">电话</th>
						<th class="hidden-xs hidden-sm">生日</th>
						<th>角色</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="hidden-xs user-img">
							<img src="../img/users/user_default.png" class="img-circle" />
						</td>
						<td>admin</td>
						<td>管理员A</td>
						<td class="hidden-xs">帅哥</td>
						<td class="hidden-xs hidden-sm">13999999999</td>
						<td class="hidden-xs hidden-sm">2000-12-03</td>
						<td>
							<span class="glyphicon glyphicon-user text-primary"></span>
							<span class="hidden-xs">管理员</span>
						</td>
						<td class="dropdown" width="80">
							<button data-toggle="dropdown" class="btn btn-xs btn-primary">
								<span>管理</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="#modal-show-user" data-toggle="modal">详细</a>
								</li>
								<li>
									<a href="#modal-edit-user" data-toggle="modal">编辑</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="" data-del="">
										<span class="text-danger">删除</span>
									</a>
								</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td class="hidden-xs user-img">
							<img src="../img/users/user_default.png" class="img-circle" />
						</td>
						<td>master</td>
						<td>管理员B</td>
						<td class="hidden-xs">帅哥</td>
						<td class="hidden-xs hidden-sm">13888888888</td>
						<td class="hidden-xs hidden-sm">2013-12-03</td>
						<td>
							<span class="glyphicon glyphicon-user text-primary"></span>
							<span class="hidden-xs">管理员</span>
						</td>
						<td class="dropdown" width="80">
							<button data-toggle="dropdown" class="btn btn-xs btn-primary">
								<span>管理</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="#modal-show-user" data-toggle="modal">详细</a>
								</li>
								<li>
									<a href="#modal-edit-user" data-toggle="modal">编辑</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="" data-del="">
										<span class="text-danger">删除</span>
									</a>
								</li>
							</ul>
						</td>
					</tr>
					<tr>
						<td class="hidden-xs user-img">
							<img src="../img/users/user1.jpg" class="img-circle" />
						</td>
						<td>usera</td>
						<td>用户A</td>
						<td class="hidden-xs">美女</td>
						<td class="hidden-xs hidden-sm">13666666666</td>
						<td class="hidden-xs hidden-sm">2012-09-08</td>
						<td>
							<span class="glyphicon glyphicon-user text-muted"></span>
							<span class="hidden-xs">用户</span>
						</td>
						<td class="dropdown" width="80">
							<button data-toggle="dropdown" class="btn btn-xs btn-primary">
								<span>管理</span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li>
									<a href="#modal-show-user" data-toggle="modal">详细</a>
								</li>
								<li>
									<a href="#modal-edit-user" data-toggle="modal">编辑</a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="" data-del="">
										<span class="text-danger">删除</span>
									</a>
								</li>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<!-- 用户详细模态框部分 -->
		<div id="modal-show-user" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" data-dismiss="modal">
							<span class="glyphicon glyphicon-remove"></span>
							<span class="sr-only">关闭</span>
						</button>
						<div class="modal-title">
							<span class="glyphicon glyphicon-th-list"></span>
							<span>用户详细</span>
						</div>
					</div>
					<div class="modal-body">
						<form action="" class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-4">
									<img src="../img/users/user1.jpg" class="img-thumbnail img-responsive img-circle" />
								</div>
								<div class="col-sm-8">
									<div class="container-fluid">
										<div class="form-group">
											<label class="control-label col-xs-3">用户名</label>
											<div class="col-xs-9">
												<div class="form-control-static">usera</div>
											</div>

											<label class="control-label col-xs-3">姓名</label>
											<div class="col-xs-9">
												<div class="form-control-static">用户A</div>
											</div>

											<label class="control-label col-xs-3">性别</label>
											<div class="col-xs-9">
												<div class="form-control-static">美女</div>
											</div>

											<label class="control-label col-xs-3">生日</label>
											<div class="col-xs-9">
												<div class="form-control-static">2012-09-08</div>
											</div>

											<label class="control-label col-xs-3">角色</label>
											<div class="col-xs-9">
												<div class="form-control-static">用户</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
		<!-- //用户详细模态框部分结束 -->
		
		<!-- 新增用户模态框部分 -->
		<div id="modal-add-user" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" data-dismiss="modal">
							<span class="glyphicon glyphicon-remove"></span>
							<span class="sr-only">关闭</span>
						</button>
						<div class="modal-title">
							<span class="glyphicon glyphicon-plus"></span>
							<span>新增用户详细</span>
						</div>
					</div>
					<div class="modal-body">
						<form id="form-add-user" action="" class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-4 text-center">
									<label>
										<input type="file" class="hidden" />
										<img src="../img/users/user_default.png" class="img-thumbnail img-responsive img-circle center-block"/>
									</label>
									<div class="text-muted">
										*点击设置头像
										<hr class="visible-xs" />
									</div>
								</div>
								
								<div class="col-sm-8">
									<div class="container-fluid">
										<div class="form-group form-group-sm">
											<label class="control-label col-xs-3">用户名</label>
											<div class="col-xs-9">
												<input type="text" class="form-control" required="required" />
											</div>
										</div>

										<div class="form-group form-group-sm">
											<label class="control-label col-xs-3">姓名</label>
											<div class="col-xs-9">
												<input type="text" class="form-control" required="required" />
											</div>
										</div>

										<div class="form-group form-group-sm">
											<label class="control-label col-xs-3">性别</label>
											<div class="col-xs-9">
												<div class="form-control-static">
													<label>
														<input type="radio" name="sex" checked="checked" class="radio-inline" />
														<span>帅哥</span>
													</label>
													<span>&nbsp;</span>
													<label>
														<input type="radio" name="sex" class="radio-inline" />
														<span>美女</span>
													</label>
												</div>
											</div>
										</div>

										<div class="form-group form-group-sm">
											<label class="control-label col-xs-3">生日</label>
											<div class="col-xs-9">
												<input type="date" class="form-control" />
											</div>
										</div>

										<div class="form-group form-group-sm">
											<label class="control-label col-xs-3">角色</label>
											<div class="col-xs-9">
												<div class="form-control-static">
													<label>
														<input type="radio" name="authority" checked="checked" class="radio-inline" />
														<span>用户</span>
													</label>
													<span>&nbsp;</span>
													<label>
														<input type="radio" name="authority" class="radio-inline" />
														<span>管理员</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit" form="form-add-user">新增用户</button>
						<button class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
		<!-- //新增用户模态框部分结束 -->

		<!-- 编辑用户模态框部分 -->
		<div id="modal-edit-user" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button class="close" data-dismiss="modal">
							<span class="glyphicon glyphicon-remove"></span>
							<span class="sr-only">关闭</span>
						</button>
						<div class="modal-title">
							<span class="glyphicon glyphicon-plus"></span>
							<span>编辑用户详细</span>
						</div>
					</div>
					<div class="modal-body">
						<form id="form-edit-user" action="" class="form-horizontal">
							<div class="form-group">
								<div class="col-sm-4 text-center">
									<label>
										<input type="file" class="hidden" />
										<img src="../img/users/user1.jpg" class="img-thumbnail img-responsive img-circle center-block"/>
									</label>
									<div class="text-muted">
										*点击更新头像
										<hr class="visible-xs" />
									</div>
								</div>
								<div class="col-sm-8">
									<div class="container-fluid">
										<div class="form-group form-group-sm">
											<label class="control-label col-xs-3">用户名</label>
											<div class="col-xs-9">
												<div class="form-control-static">usera</div>
											</div>

											<label class="control-label col-xs-3">姓名</label>
											<div class="col-xs-9">
												<input type="text" class="form-control" required="required" value="用户A" />
											</div>

											<label class="control-label col-xs-3">性别</label>
											<div class="col-xs-9">
												<div class="form-control-static">
													<label>
														<input type="radio" name="sex" class="radio-inline" />
														<span>帅哥</span>
													</label>
													<span>&nbsp;</span>
													<label>
														<input type="radio" name="sex" checked="checked" class="radio-inline" />
														<span>美女</span>
													</label>
												</div>
											</div>

											<label class="control-label col-xs-3">生日</label>
											<div class="col-xs-9">
												<input type="date" class="form-control" value="2012-09-08" />
											</div>

											<label class="control-label col-xs-3">角色</label>
											<div class="col-xs-9">
												<div class="form-control-static">
													<label>
														<input type="radio" name="authority" checked="checked" class="radio-inline" />
														<span>用户</span>
													</label>
													<span>&nbsp;</span>
													<label>
														<input type="radio" name="authority" class="radio-inline" />
														<span>管理员</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" type="submit" form="form-edit-user">更新用户</button>
						<button class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
		<!-- //编辑用户模态框部分结束 -->

		<!-- //页面主体部分结束 -->

		<!-- 页面脚部部分 -->
		<footer>
			<hr/>
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="col-sm-6 auth">
							<img src="../img/foot_img3.png" class="img-responsive center-block sm-right" alt="">
						</div>
						<div class="col-sm-6 auth">
							<img src="../img/foot_img4.png" class="img-responsive center-block sm-left" alt="">
						</div>
						<div class="clearfix"></div>
						<div class="copy">Rights Reserved 免费服务热线: 400-705-1111 | 固话也可拨打: 800-705-1111 E-Mail: service@happygo.com</div>
						<div class="copy">湘ICP备12000157号 信息网络传播视听节目许可证号：1810530</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- //页面脚部部分结束 -->

	</body>

</html>