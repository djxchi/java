<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>快乐购</title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="css/normalize.min.css" />
		<link rel="stylesheet" type="text/css" href="css/default.css" />

		<!-- JS -->
		<script src="js/jquery-3.2.1.min.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>

		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js" type="text/javascript" charset="utf-8"></script>
			<script src="js/respond.min.js" type="text/javascript" charset="utf-8"></script>
		<![endif]-->
	</head>

	<body>
		<!-- 导航条开始 -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<a href="#navbar-menu1" data-toggle="collapse" class="navbar-toggle collapsed">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<a href="index.jsp" class="navbar-brand">
						<img src="img/logo.png" alt="Happy Go" />
					</a>
				</div>
				<div id="navbar-menu1" class="navbar-collapse collapse">
					<div class="navbar-text">
						<span>Admin，</span>
						<span>欢迎来到快乐购！&nbsp;</span>
						<div class="visible-xs"></div>
						<span>[<a href="login.jsp">登录</a>]&nbsp;</span>
						<span>[<a href="register.jsp">注册</a>]&nbsp;</span>
						<span>[<a href="login.jsp">注销</a>]&nbsp;</span>
					</div>

					<div class="navbar-right">
						<ul class="navbar-nav nav">
							<li class="active">
								<a href="index.jsp">首页</a>
							</li>
							<li>
								<a href="my.jsp">个人资料</a>
							</li>
							<li>
								<a href="order.jsp">我的快乐购</a>
							</li>
							<li>
								<a href="cart.jsp" data-flag="link-cart">购物车</a>
							</li>
							<li>
								<a href="html/index.jsp">后台管理</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<!-- //导航条结束 -->

		<!-- 滚动广告部分 -->
		<div id="carousel-generic" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-generic" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-generic" data-slide-to="1"></li>
				<li data-target="#carousel-generic" data-slide-to="2"></li>
				<li data-target="#carousel-generic" data-slide-to="3"></li>
				<li data-target="#carousel-generic" data-slide-to="4"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="img/slides/web-101-101-1.jpg" alt="广告I" />
					<div class="carousel-caption">
						<h4>广告I</h4>
					</div>
				</div>
				<div class="item">
					<img src="img/slides/web-101-101-2.jpg" alt="广告II" />
					<div class="carousel-caption">
						<h4>广告II</h4>
					</div>
				</div>
				<div class="item">
					<img src="img/slides/web-101-101-3.jpg" alt="广告II" />
					<div class="carousel-caption">
						<h4>广告II</h4>
					</div>
				</div>
				<div class="item">
					<img src="img/slides/web-101-101-4.jpg" alt="广告III" />
					<div class="carousel-caption">
						<h4>广告III</h4>
					</div>
				</div>
				<div class="item">
					<img src="img/slides/web-101-101-5.jpg" alt="广告IV" />
					<div class="carousel-caption">
						<h4>广告IV</h4>
					</div>
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-generic" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-generic" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!-- //滚动广告部分结束 -->
		
		<!-- 商品分类部分 -->
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<li>
						<a href="">全部</a>
					</li>
					<li class="dropdown">
						<a href="products.jsp" data-toggle="dropdown">
							<span>女装/内衣</span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="products.jsp">全部</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="products.jsp">毛呢外套</a>
							</li>
							<li>
								<a href="products.jsp">羽绒服</a>
							</li>
							<li>
								<a href="products.jsp">棉服</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="" data-toggle="dropdown">
							<span>男装/运动户外</span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="products.jsp">全部</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="products.jsp">单西</a>
							</li>
							<li>
								<a href="products.jsp">棉衣</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="" data-toggle="dropdown">
							<span>女鞋/男鞋/箱包</span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="products.jsp">全部</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="products.jsp">平底单鞋</a>
							</li>
							<li>
								<a href="products.jsp">高跟单鞋</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="" data-toggle="dropdown">
							<span>手机/数码/电脑办公</span>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="products.jsp">全部</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="products.jsp">笔记本</a>
							</li>
							<li>
								<a href="products.jsp">平板电脑</a>
							</li>
							<li>
								<a href="products.jsp">台式机</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- //商品分类部分结束 -->
		
		<!-- 搜索表单部分 -->
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<img src="img/top_center.jpg" class="img-responsive" alt="" />
				</div>
				<div class="col-sm-4">
					<form action="products.jsp">
						<div class="form-group form-group-sm">
							<div class="input-group input-group-sm">
								<input type="search" class="form-control" placeholder="输入商品名称..." />
								<div class="input-group-btn">
									<button class="btn btn-default">
										<span class="glyphicon glyphicon-search"></span>
										<span class="sr-only">搜索</span>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- //搜索表单部分结束 -->

		<!-- 页面主体部分 -->
		<div class="container">

			<div class="row">
				<div class="col-sm-9">
					<h3 class="page-header">
          <span>全场低价商品</span>
          <small>精致好物 更快乐的生活</small>
					<small>
						<a href="products.jsp">更多...</a>
					</small>
        </h3>

					<!-- 商品列表部分 -->
					<div class="container-fluid">
						<div class="row">
							<!-- 商品一 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/a.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">棉衣女中长款韩版2017冬装新款羽绒棉服大毛领修身百搭棉袄外套潮</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>158.00</span>
								</div>
							</div>

							<!-- 商品二 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/b.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">Five Plus新女冬装刺绣双排扣长款长袖毛呢西装外套2HD5345220</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>339.00</span>
								</div>
							</div>

							<!-- 商品三 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/c.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">2017秋冬新款无双面羊绒大衣女短款韩版潮小个子羊毛呢子外套加厚</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>398.00</span>
								</div>
							</div>

							<!-- 商品四 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/d.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">圣其罗-毛呢大衣女中长款2017新款双面呢子大衣羊毛外套秋冬大衣</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>808.00</span>
								</div>
							</div>
						</div>

						<div class="row">
							<!-- 商品五 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/e.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">2017秋冬新双面呢大衣女修身风衣手工无羊绒尼妮子毛呢外套中长款</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>1,060.00</span>
								</div>
							</div>

							<!-- 商品六 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/f.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">王小鸭双面羊绒大衣女中长款2017新款韩版潮长袖秋冬装毛呢外套女</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>1,398.00</span>
								</div>
							</div>

							<!-- 商品七 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/g.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">Lenovo/联想 拯救者 R720-15IKB游戏笔记本四核手提电脑15.6英寸</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>5,499.00</span>
								</div>
							</div>

							<!-- 商品八 -->
							<div class="col-sm-3 product-item">
								<div class="img-thumbnail">
									<div>
										<a href="product.jsp">
											<img src="img/products/h.png" class="img-responsive img-rounded" />
										</a>
									</div>
								</div>
								<div class="title">
									<a href="products.jsp">Apple MacBook Air 13.3英寸笔记本电脑I5 8G 128G MQD32CH/A银色</a>
								</div>
								<div class="price">
									<span>￥</span>
									<span>6,058.00</span>
								</div>
							</div>
						</div>
					</div>
					<!-- //商品列表部分结束 -->

				</div>
				<div class="col-sm-3">
					<h3 class="page-header">
					<span>新闻公告</span>
					<small>
						<a href="news.jsp">更多...</a>
					</small>
				</h3>
					<div class="container-fluid">
						<ul class="news">
							<li>
								<a href="news.jsp">
									<span class="pull-left">迎双旦，促销大酬宾</span>
									<span class="pull-right">2010/12/24</span>
								</a>
							</li>
							<li>
								<a href="news.jsp">
									<span class="pull-left">加入会员，赢千万大礼包</span>
									<span class="pull-right">2017/12/31</span>
								</a>
							</li>
							<li>
								<a href="news.jsp">
									<span class="pull-left">免费送</span>
									<span class="pull-right">2017/12/31</span>
								</a>
							</li>
							<li>
								<a href="news.jsp">
									<span class="pull-left">迎双旦，促销大酬宾，迎双旦，促销大酬宾</span>
									<span class="pull-right">2010/12/24</span>
								</a>
							</li>
							<li>
								<a href="news.jsp">
									<span class="pull-left">加入会员，赢千万大礼包</span>
									<span class="pull-right">2017/12/31</span>
								</a>
							</li>
							<li>
								<a href="news.jsp">
									<span class="pull-left">免费送</span>
									<span class="pull-right">2017/12/31</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

		</div>
		<!-- //页面主体部分结束 -->

		<!-- 页面脚部部分 -->
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-2 tip-item">
						<img src="img/foot_img_tip1.png" class="img-responsive center-block">
						<div class="h4 text-danger">上市企业</div>
						<div class="h5 text-muted">股票代码000001</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 tip-item">
						<img src="img/foot_img_tip2.png" class="img-responsive center-block">
						<div class="h4 text-danger">快乐购严选</div>
						<div class="h5 text-muted">正品行货 品质保证</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 tip-item">
						<img src="img/foot_img_tip3.png" class="img-responsive center-block">
						<div class="h4 text-danger">10天无理由退货</div>
						<div class="h5 text-muted">为您提供售后无忧保障</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 tip-item">
						<img src="img/foot_img_tip4.png" class="img-responsive center-block">
						<div class="h4 text-danger">满129免邮</div>
						<div class="h5 text-muted">在线支付 购物包邮</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 tip-item">
						<img src="img/foot_img_tip5.png" class="img-responsive center-block">
						<div class="h4 text-danger">微笑无时无刻</div>
						<div class="h5 text-muted">365*24小时人工服务</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 tip-item">
						<img src="img/foot_img_tip6.png" class="img-responsive center-block">
						<div class="h4 text-danger">闪电物流</div>
						<div class="h5 text-muted">闪电发货 次日必达</div>
					</div>
				</div>
			</div>
			<hr/>
			<div class="container">
				<div class="row">
					<div class="col-xs-4 col-sm-2 f-link">
						<a href="#">使用帮助</a>
						<a href="#">税费收取规则</a>
						<a href="#">新手指南</a>
						<a href="#">常见问题</a>
						<a href="#">用户协议</a>
					</div>
					<div class="col-xs-4 col-sm-2 f-link">
						<a href="#">使用帮助</a>
						<a href="#">税费收取规则</a>
						<a href="#">新手指南</a>
						<a href="#">常见问题</a>
						<a href="#">用户协议</a>
					</div>
					<div class="col-xs-4 col-sm-2 f-link">
						<a href="#">使用帮助</a>
						<a href="#">税费收取规则</a>
						<a href="#">新手指南</a>
						<a href="#">常见问题</a>
						<a href="#">用户协议</a>
					</div>
					<div class="col-xs-4 col-sm-2 f-link">
						<a href="#">使用帮助</a>
						<a href="#">税费收取规则</a>
						<a href="#">新手指南</a>
						<a href="#">常见问题</a>
						<a href="#">用户协议</a>
					</div>
					<div class="col-xs-4 col-sm-2 f-link">
						<div>手机快乐购</div>
						<img src="img/footer_ewm_01.png" alt="">
						<div>下载移动客户端</div>
					</div>
					<div class="col-xs-4 col-sm-2 f-link">
						<div>快乐微信购</div>
						<img src="img/footer_ewm_01.png" alt="">
						<div>快乐购官方微信</div>
					</div>
				</div>
				<div class="row">
					<div class="text-center">
						<div class="col-sm-6 auth">
							<img src="img/foot_img3.png" class="img-responsive center-block sm-right" alt="">
						</div>
						<div class="col-sm-6 auth">
							<img src="img/foot_img4.png" class="img-responsive center-block sm-left" alt="">
						</div>
						<div class="clearfix"></div>
						<div class="copy">Rights Reserved 免费服务热线: 400-705-1111 | 固话也可拨打: 800-705-1111 E-Mail: service@happygo.com</div>
						<div class="copy">湘ICP备12000157号 信息网络传播视听节目许可证号：1810530</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- //页面脚部部分结束 -->

	</body>

</html>